# IIOT PROTOCOLS  

### 4-20mA  

There are several types of inputs in the world of process control. Digital signals like the ModBus, provide control over variables and displays. The analog signals are the ones that carry information about the process in varying amounts of current and voltage.  Out of all possible analog signals that can be used in transmitting the information, the _4-20 mA_ loop is a standard one used in the industries.  

> _Process Control_: The active change of the process based on the process monitoring is called process control.  

Before the use of electronic circuitry in the insutries, the process control used mechanical operations like the the measurement of pressure. The facilities used pneumatic controllers where these controllers were powered by varying the pressure. For some reasons, the standard pressure range became 3-15 psi. The thing to be noted here is:

    * It was expensive to measure pressure below 3 psi.    
    * Signals below 3 psi was unrecognizable.  
    * A 3 psi signal meant it was a live zero signal while a 0psi meant it was a failure in the system.   
    * After electronic circuitry was implemented in the insutriesthe 4-20 mA loop became the standard like the 3-15 psi.  

### Working of 4-20 mA  

According to Ohm's law, **V = I x R**. So for in a given circuit, with some resistance and voltage input, there will voltage given by a component while there will be voltage drop across a component. But the current through the circuit remains constant. This is the principle that the **4-20mA** loop works on. For example, the flow consider the water flow throughout your house.   
    * When a tap is opened in the house, the water flows through the pipe like the current flows through the circuit.  
    * The voltage that makes the current flow in the circuit is like the pressure that makes the water flow in the pipes.  
    * The flow of water through the taps is limited by the dimensions of the tap. The resistors are like the taps, that limits the current flow.  
    * The flow of water however in the pipes is cthe same, like the flow of current in the circuit.  
That is why current is used in sending information as signals so reliable.  
![1][one]

[one]: https://1oomzzme3s617r8yzr8qutjk-wpengine.netdna-ssl.com/wp-content/uploads/2018/09/WHTH_FAQ_Analog_current-loop_Pt1_Fig1.png  

**Working:**  
1. _Sensor_: For measuring the process variables, usually temperature, pressure, humidity, sensors are used.  
2. _Transmitter_: Whatever the sensor measures, the transmitter converts it into the current signal. For example, if a sensor measures the height of a tank. The current signal transmitted is 4mA of 0 feet high and 20mA for the maximum height of the tank.  
3. For the signal to be produced, a power source that can produce DC output is used. The voltage chosen must be at least 10% more than the voltage drop across the different components, since improper design might lead to failures.  
4. The _loop_ is the part that connects the transmitter and the device and then from the device to the transmitter. This refers the wire that connects them. The current in the loop is controlled by the signal that the transmitter produces according to the sensor's measurements. The wire itself offers some resistance which is very small in short distances. But when the loop part stretches for long distances, this might add up.  
5. There is device somewhere in the loop that receives the signal from the loop and interprets it so that it can be put to some use. With the signal received, the device displays it after interpreting or uses it for controlling some other components.  

This is all what it takes for a 4-20mA circuit to be complete. A sensors measures the physical environment and sends it to the transmitter. The transmitter converts into a current signal ranging from 4-20 mA and is transferred through a loop to the receiver that interprets it and uses the information.  
##### Pro's and Con's  

Pro's:                                                            
    * A standard used in many industries                              
    * Simple and easy circuit with less wirings.  
    * Since 4mA equals 0%, easy to detect faults.  
    * Better for covering long distances.  
    * Less prone to electrical noise.

Con's:
    * Current loops can transmit only one process signal.  
    * If there are many process variables then many loops are required.  
    * If the number of loops increase, the number of wires increase and isolation of wires increase.  
    * Due to this the wirings become complex.  


# Modbus Communication Protocol  

* ModBus is a Communication protocol used in programmable logic devices. It is usually from the instrumentation and other control devices to the the main controller or where the data is gathered.  
* It is used in the connection between the electronic devices.  
* It is based on the _Master_ and _slave_ where the device requesting information is the master and the device supplying the information is the slave.  
* In ModBus protocol there can be 247 slaves and 1 master.  
* The slave addressed range from 1 to 247.  
* The communication works in a frame that occurs in a function code.  
* The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.  
* The slave responds to what is receives.  

This protocol is used in IoT as a local interface to manage the devices. The ModBus protocol can be used over 2 interfaces:  
    1. **RS485** - called ModBus RTU  
    2. **Ethernet** - called ModBus TCP/IP  
The RS485 is a serial transmission standard like the UART. Several RS485 devices can be put together.But the RS485 requires a proper interface as it is not directly compatible. So a RS485 to USB interface is used.  



