# 1. Power Supply

**There are some Do's and Don'ts when using the power supply.**  

### Do's  

    1. When connecting the power supply always be sure that the input voltage of the board and the output of the power supply is the same.  
    2. Before connecting the power supply be sure that the circuit connections are correct.  

### Don'ts  

    1. Don't connect power supplies to the boards that don't match with the rating.  
    2. Don't connect higher power(12V) to lower power(5V).  
    3. Don't force a connector into the socket. This might damage the connector.  

# 2. Handling  

**Some things are to taken care of when handling the equipments.**  

### Do's  

    1. Always disconnect power when operating on the equipments.  
    2. Even if a board is not connected to tthe power supply, treat it as if the board is powered.  
    3. Keep the board on a flat surface when working on them like on a wooden table.  
    4. Make sure the electrical equipments are operated using dry hands.  
    5. Make sure all the electrical points are enclosed.  
    6. To reduce the heat if the board gets hot, use a dc usb fan.  

 ### Don'ts  

    1. Don't handle when the power is ON.  
    2. Don't touch the boards or any electrical equipment with wet hands.  
    3. Don't touch any metal to the development board.  

# 3. GPIO  

### Do's  

    1. Find out whether the pins support 5V or 3.3V .  
    2. Use a logic level converter for using 5V with 3.3V .  
    3. Connect the sensors, LED to the correct pins with suitable resistors.  

### Don'ts  

    1. Don't connect motors directly, instead use transistors to run them.  
    2. Never connect anything more than 5V to 3.3V  pins.  
    3. Don't plug in high or negatove voltage.  
    4. Don't make connections while the board is running.  

# Guidelines for using interfaces(I2C, SPI, UART)  

### 1. UART

    * Connec the Tx pin of the device 1 to Rx pin of the device 2. Then the Rx pin of device 1 to the Tx pin of device 2. 
    * The UART communication uses a USB to TTL converter. 
    * If one of the deivces is 5V and the other is 3.3V then a logic level converter is required.  
    * Sensor interfacing with UART communication might require a protection circuit.  

### 2. I2C  

    * While using the SDA and SCL protection circuit is required.  
    * This is done by using pull up registers.  
    * If in-built pull up registers, external not required.
    * If the sensors are connected on a bread-board for prototyping, pull up resistors are used.  
    * Usually, the resitance of the pull up resistorslie between 2.2kohm - 4kohm.  

### 3. SPI  

    * The SPI protocol works in the push-pull mode.  
    * This doesnt usually need protection circuit.  
    * If one device connects to multiple slaves in SPI, there is a chance for a slave2 can _hear and respond_ to the information sent to slave1.  
    * To overcome this, pull up resistors with value between 1Kohm ~ 10Kohm.  Usually 4.7KOhm resistor is used.  
    