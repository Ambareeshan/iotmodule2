# Sensors and Actuators

**Sensors**, also called as the transducers, convert the data obtained from the physical world into electrical signals. The data obtained is given to the system as electric signals and is interpreted. For example, a RTD is a temperature sensor that can be used to measure the temperature according to the changes in the resistance. Similarly some other sensors like DHT11 and DHT22 can be used to find temperature and humidity and also integrated with IoT to send the data to the cloud or view it.    
![sensor][images]

[images]: https://media.geeksforgeeks.org/wp-content/uploads/20200512075711/sensor.png

**Actuators**, are also transducers, but act in the reverse direction of the sensors. The actuators work according to how the sensors and actuators are linked. For example, considering the example of a temperature controlled dc fan, we measure the temperature using a suitable sensor and once the temperature reaches a minimum value, we switch on the fan. Here the dc fan is the actuator and the the device used to measure the temperature is the sensor used.  
  
![actuators][photo]

[photo]: https://media.geeksforgeeks.org/wp-content/uploads/20200512075727/actuator.png

![image][link]

[link]: https://www.avsystem.com/media/top_sensor_types_used_in_iot-02.png

* Examples of sensors are proximity sensors, light sensors, moisture sensors, gyroscope sensors, water level sensors etc.  
* Examples of actuators are motors, linear actuators, relays, solenoids.  

# Analog and Digital signals

**Analog signals** are continuous signals which represent physical systems. It is denoted by sine waves. The signals are continuous over time. For example, the human voice is a analog signal. The analog inputs record the waveforms as they are present.  

**Digital Signals** are discrete signals. This means that the values are either 0 or a 1. Usually, the 0 is denoted as LOW and 1 is denoted as HIGH. These are denoted by square waves. The digital signals are used in PCs to send binary information as the binary system consists of 1 and 0. The digital signals are produced by digital modulation.  

![singals][type]

[type]: https://www.allaboutcircuits.com/uploads/articles/An-Introduction-to-Digital-Signal-Processing-(1).png

# MicroControllers vs MicroProcessors

* **Microcontrollers** are more strong in the I/O pins. This makes it easier for them drive the actuators or sensors connected to the board. The microcontrollers have limited or finite flah memory which may be upto 2Mb depending on the board. The Microcontrollers don't have firmware, operating system. The code written in the IDE is directly uploaded to the microcontroller and the microcontroller works according to the program. The startup time for the microcontrollers is less. The microcontrollers consume less power and have less RAM. For example the **arduino UNO** is a microcontroller with 2Kb RAM and 32Kb Flash. The output on each IO pins is upto 40 mA.  

* **MicroProcessors** on the other hand have strong processing power. The I/O pins are weak, which means, some external components like transistors have to be connected to the board to drive external peripherals. The MicroProcessors act as the central processing units. For example, the **Raspberry Pi** is a MicroProcessor. They are also called as _Single Board Computers_. The power consumption is more than microcontrollers. In the raspberry pi board, the I/O pins can give out only 5-10 mA. Also, the programs are written in an operating system. In most cases it is Linux. They Flash memory and RAM of the microporcessors are higher than the microcontrollers. For example the RAM of Raspberry PI 4 Model B comes in different variants like 4Gb, 8Gb etc.  

![uC vs uP][uC]

[uC]: https://i.all3dp.com/cdn-cgi/image/fit=cover,w=1284,h=722,gravity=0.5x0.5,format=auto/wp-content/uploads/2019/02/06122534/Arduino-versus-Raspberry-Pi.jpg 

# Introduction to Raspberry Pi  

![rpi][rpi1]

[rpi1]: https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Drawing_of_Raspberry_Pi_model_B_rev2.svg/1280px-Drawing_of_Raspberry_Pi_model_B_rev2.svg.png 

* The Raspberry Pi is a single board computer. Even though it consumes less power, it is very powerfull. It is a System on Chip(SoC). The Raspberry pi has no onboard storage and thus an external sd card will be inserted on the board.   
* It is a microprocessor and OS like Linux can be loaded on it. The Pi uses ARM architecture chip. Many Pi's can be connected together. External shields can be connected to the board. The board can be connected to sensors and actuators.    
* _Interfaces_ are ways to connect a sensor to a microprocessor.The Raspberry pi has the following interfaces:  
    * GPIO
    * SPI
    * UART
    * I2C
    * PWM
The SPI, I2C, and UART are communication protocols. The UART is asynchronous while the other two are synchronous. The SPI uses more than 4 wires out of which, the MOSI(Master Out Slave In), MISO(Master In Slave Out), SCLK are common. The I2C uses 2 wires- SDA(data), SCL(clock). The UART has two pins-tx(transmitter), Rx(eceiver).  


# Serial And Parallel  


* The Serial communication is when the data is sent bit by bit. All the data is not transfered at the same time. The data bits are sent on one specific order. This order is important since even a slight change can disturb the information carried.  

* The Parallel communication is when all the data bits are sent at the same time through different channels. This means that data can be sent faster than when using Serial communication. This is used when large amount of data must be sent.  

Examples of Serial communication:  
    1. I2C  
    2. SPI  
    3. UART  

Example of Parallel communication: GPIO  




